<!--
SPDX-FileCopyrightText: 2024 Matija Radovic <radovic@tuta.io>

SPDX-License-Identifier: GPL-3.0-only
-->

# HW and SW Design Of Digital Soldering Station(for jbc T245 handle)

- Hardware side of this project is specialy designed for soldering handles with tips(cartridges) that have heater and temperature sensor connected in series.

- Software is adaptable for any soldering handle available on market.

**3d render of PCB:**

![pcb-3d-render](https://codeberg.org/attachments/b7b10918-d129-4bad-962d-bd0c639eebc2)

## Build firmware

- Navigate to `firmware/` folder: `cd firmware`
- Execute `make all` to build or you can run `make flash` to build and upload the newly built firmware at once.