#ifndef LCD_H
#define LCD_H

#include <stdint.h>
#include "types.h"

#define CLEAR_AND_HOME_DELAY_IN_MS 2.0f
#define FOUR_BIT_MODE_INIT_DELAY_IN_MS 4.2f
#define FRAME_DELAY_IN_US 150.0f
#define POWERUP_DELAY_IN_MS 50.0f
#define ENABLE_PULSE_DURATION_IN_US 1.0f

#define SECOND_LINE_START_ADDRESS 0x40u

#define RS 0u
#define RW 1u
#define ENABLE 2u
#define BL 3u

#define BACK_LIGHT_ON 1u

#define LCD_ROWS 2u
#define LCD_COLUMNS 16u

#define LCD_DELAY_BETWEEN_FRAMES 1u // ms

// +-------------------+
// | LCD INSTRUCTIONS  |
// +-------------------+

// Clear display
// Clears entire display and display sets DDRAM address 0 in address counter
#define INSTR_CLEAR_DISPLAY 0b00000001u

// Return home
// Sets DDRAM address 0 in address counter; Also returns display from being shifted to original position; DDRAM contents remain unchanged
#define INSTR_RETURN_HOME 0b00000010u

// Entry mode set: | 0 | 0 | 0 | 0 | 0 | 1 | I/D | S |
//                   7   6   5   4   3   2    1    0
// Sets cursor move direction and specifies display shift; These operations are performed during data write and read
#define INSTR_ENTRY_MODE_SET_INCREMENT_ACCOMPANIES_DISPLAY_SHIFT 0b00000111u
#define INSTR_ENTRY_MODE_SET_DECREMENT_ACCOMPANIES_DISPLAY_SHIFT 0b00000101u
#define INSTR_ENTRY_MODE_SET_DECREMENT                           0b00000100u
#define INSTR_ENTRY_MODE_SET_INCREMENT                           0b00000110u


// Display on/off control: | 0 | 0 | 0 | 0 | 1 | D | C | B |
//                           7   6   5   4   3   2   1   0
// Sets entire display (D) on/off,cursor on/off (C), and blinking of cursor position character (B)
#define INSTR_DISPLAY_ON_CURSOR_BLINKING      0b00001111
#define INSTR_DISPLAY_ON_CURSOR_NOBLINKING    0b00001110
#define INSTR_DISPLAY_ON_NOCURSOR_BLINKING    0b00001101
#define INSTR_DISPLAY_ON_NOCURSOR_NOBLINKING  0b00001100
#define INSTR_DISPLAY_OFF_CURSOR_BLINKING     0b00001011
#define INSTR_DISPLAY_OFF_CURSOR_NOBLINKING   0b00001010
#define INSTR_DISPLAY_OFF_NOCURSOR_BLINKING   0b00001001
#define INSTR_DISPLAY_OFF_NOCURSOR_NOBLINKING 0b00001000

// Cursor or display shift: | 0 | 0 | 0 | 1 | S/C | R/L | 0 | 0 |
//                            7   6   5   4    3     2    1   0
// Moves cursor and shifts display without changing DDRAM contents
#define INSTR_CURSOR_MOVE_TO_THE_LEFT    0b00010000
#define INSTR_CURSOR_MOVE_TO_THE_RIGHT   0b00010100
#define INSTR_DISPLAY_SHIFT_TO_THE_LEFT  0b00011000
#define INSTR_DISPLAY_SHIFT_TO_THE_RIGHT 0b00011100

// Function set: | 0 | 0 | 1 | DL | N | F | 0 | 0 |
//                 7   6   5   4    3   2   1   0
// Sets interface data length (DL), number of display lines (N), and character font (F)
#define INSTR_FUNCTION_SET_4BIT_1LINE_8x5FONT  0b00100000
#define INSTR_FUNCTION_SET_4BIT_1LINE_8x10FONT 0b00100100
#define INSTR_FUNCTION_SET_4BIT_2LINE_8x5FONT  0b00101000
#define INSTR_FUNCTION_SET_4BIT_2LINE_8x10FONT 0b00101100
#define INSTR_FUNCTION_SET_8BIT_1LINE_8x5FONT  0b00110000
#define INSTR_FUNCTION_SET_8BIT_1LINE_8x10FONT 0b00110100
#define INSTR_FUNCTION_SET_8BIT_2LINE_8x5FONT  0b00111000
#define INSTR_FUNCTION_SET_8BIT_2LINE_8x10FONT 0b00111100

// Set CGRAM address: | 0 | 1 | ACG5 | ACG4 | ACG3 | ACG2 | ACG1 | ACG0 |
//                      7   6    5      4      3      2      1      0
//  Sets CGRAM address; CGRAM data is sent and received after this setting
#define INSTR_SET_CGRAM_ADDRESS(ACG) (0b01000000 | ((uint8_t)(ACG) & 0b00111111))

// Set DDRAM address: | 1 | ADD6 | ADD5 | ADD4 | ADD3 | ADD2 | ADD1 | ADD0 |
//                      7    6      5      4      3      2      1      0
//  Sets DDRAM address; DDRAM data is sent and received after this setting
#define INSTR_SET_DDRAM_ADDRESS(ADD) (0b10000000 | ((uint8_t)(ADD) & 0b01111111))

// Read bussy flag & address: | BF | AC | AC | AC | AC | AC | AC | AC |
//                              7    6    5    4    3    2    1    0
//  Reads busy flag (BF) indicating internal operation is being performed and reads address counter contents
#define INSTR_READ_BUSY_FLAG_AND_ADDRESS 0b00000000

typedef enum
{
    DEGREE_SYMBOL = 0b11011111,
}t_LcdSymbols;

typedef enum
{
    INSTRUCTION_REGISTER = 0,
    DATA_REGISTER = 1
} t_RegisterSelect;

t_Result lcd_send_command(const uint8_t data, const t_RegisterSelect reg);
t_Result lcd_print(const char* str);
t_Result lcd_clear_display(void);
t_Result lcd_return_home(void);
t_Result lcd_set_cursor(const uint8_t x, const uint8_t y);
void lcd_init(void);

#endif // LCDH_H
