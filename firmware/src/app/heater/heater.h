#ifndef HEATER_H
#define HEATER_H

#include <stdint.h>

#define TEMP_GAIN 201.0f
#define K_TYPE_TC_FACTOR 0.04f
#define VOLTS_TO_MILLIVOLTS_FACTOR 1000.0f

#define MAX_DUTY_CYCLE 255

void heater_init(void);
float heater_measure_temp(void);
uint8_t heater_heat(void);
uint16_t heater_get_target_temp(void);
uint16_t heater_get_real_temp(void);

#endif /* HEATER_H */
