#ifndef GPIOS_H
#define GPIOS_H

#include <stdint.h>

#define EGPIO 0x03;

typedef enum
{
    IN = 0,
    OUT = 1,
    IN_PULLUP = 2
}t_GpioDir;

typedef enum
{
    LOW = 0,
    HIGH = 1
}t_GpioLevel;

typedef enum
{
    PIN_0 = 0,
    PIN_1 = 1,
    PIN_2 = 2,
    PIN_3 = 3,
    PIN_4 = 4,
    PIN_5 = 5,
    PIN_6 = 6,
    PIN_7 = 7
}t_GpioPin;

typedef enum
{
    PORT_B = 0x03,
    PORT_C = 0x08,
    PORT_D = 0x0B
}t_GpioPort;

void gpio_set_dir(t_GpioPort port, t_GpioPin pin, t_GpioDir dir);
void gpio_write(t_GpioPort port , t_GpioPin pin, t_GpioLevel level);
t_GpioLevel gpio_read(t_GpioPort port , t_GpioPin pin);
t_GpioDir gpio_get_dir(t_GpioPort port , t_GpioPin pin);
void gpio_toggle(t_GpioPort port , t_GpioPin pin);

#endif /* GPIOS_H */