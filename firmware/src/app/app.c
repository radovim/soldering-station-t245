#include <stdio.h>
#include <stdint.h>
#include "app.h"
#include "heater.h"
#include "lcd.h"
#include "timer.h"
#include <util/delay.h>
#include "uart.h"

char text[100];
uint8_t dc = 0;

void app_run(void)
{
    static uint32_t last_millis;
    dc = heater_heat();
    if ((timer_get_millis() - last_millis) >= 700)
    {
        /* app_display_update(); */
        sprintf(text, "Set Temp: %u | Act Temp: %u | Duty Cycle: %u\n", heater_get_target_temp(), heater_get_real_temp(), dc);
        uart_put_string(text);
        last_millis = timer_get_millis();
    }
}

void app_display_update()
{
    uint16_t target_temp = heater_get_target_temp();
    /* uint16_t real_temp = heater_get_real_temp(); */
    sprintf(text, "t: %u", target_temp);
    lcd_clear_display();
    lcd_print(text);
    lcd_send_command(DEGREE_SYMBOL, DATA_REGISTER);
    lcd_print("C");
    /* lcd_set_cursor(1,0); */
    /* sprintf(text, "t: %u", real_temp); */
    /* lcd_print(text); */
    /* lcd_send_command(DEGREE_SYMBOL, DATA_REGISTER); */
    /* lcd_print("C"); */
}
