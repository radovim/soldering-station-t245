#include <avr/io.h>
#include "adc.h"
#include <avr/interrupt.h>
#include "registers.h"


static void adc_start_continous_reading(uint8_t n);

static volatile uint16_t buffer[ADC_SAMPLE_BUFFER_MAX_SIZE] = {0};

static t_AdcContext adc_context = {
    .sample_buffer_max_size = 255,
    .reference_voltage_mv = 0,
    .adc_mode = 0,
    .sample_buffer = {
        .num_of_samples = 0,
        .sample_counter = 0,
        .status = FULL,
        .buff = buffer
        },
    };

ISR(ADC_vect)
{
    switch(adc_context.adc_mode)
    {
        case FREE_RUNNING:
            if ( adc_context.sample_buffer.sample_counter >= adc_context.sample_buffer.num_of_samples)
            {
                adc_disable_auto_triggering();
                adc_disable_interrupt();
                adc_context.sample_buffer.status = FULL;
            }
            else
            {
                adc_context.sample_buffer.buff[adc_context.sample_buffer.sample_counter++] = (uint16_t)ADCL | (uint16_t)(ADCH << 8);
            }
            break;
        case SINGLE:
            adc_disable_interrupt();
            break;
        default:
            adc_disable_interrupt();
            return;
    }
}

void adc_set_reference(t_AdcReferenceVoltage ref_voltage, uint16_t ref_volt_mv)
{
    ADMUX |= (uint8_t)(ref_voltage << REFS0);

    if (ref_voltage == INTERNAL_1V1)
    {
        adc_context.reference_voltage_mv = 1100; // 1100mv = 1.1v
    }
    else
    {
        adc_context.reference_voltage_mv = ref_volt_mv;
    }
}

t_AdcReferenceVoltage adc_get_reference(void)
{
    t_AdcReferenceVoltage ref_voltage = AREF;

    ref_voltage = (uint8_t)(ADMUX >> REFS0);

    return ref_voltage;
}

void adc_init(t_AdcReferenceVoltage ref_voltage, uint16_t ref_volt_mv)
{
    adc_set_reference(ref_voltage, ref_volt_mv);

    ADMUX |= 1 << REFS0; // AVCC
    ADMUX |= 0x3 << MUX0; // multiplex ADC3 channel
    adc_enable();
    ADCSRA |= 0x7 << ADPS0; // set ADC prescaler to 128
    adc_start_conversion(); // start first conversion(the first conversion performs initialization of ADC)
}

void adc_set_prescaler(uint8_t prescaler_value)
{
    for (uint8_t pv = 1; pv <= 7; pv++)
    {
        if ((uint8_t)(1 << pv) == prescaler_value)
        {
            ADCSRA |= 0x7 << ADPS0;
        }
    }
}

volatile uint16_t* adc_get_samples(uint8_t number_of_samples)
{
    uint8_t n = number_of_samples;
    if (n > ADC_SAMPLE_BUFFER_MAX_SIZE)
    {
        return ((void *) 0);
    }
    adc_start_continous_reading(n);
    while(adc_context.sample_buffer.status != FULL);
    return buffer;
}

uint16_t adc_read(void)
{
    adc_start_continous_reading(1);
    while(adc_context.sample_buffer.status != FULL);
    return buffer[0];
}

static void adc_start_continous_reading(uint8_t n)
{
    adc_context.sample_buffer.num_of_samples = n;
    adc_context.sample_buffer.sample_counter = 0;
    adc_context.sample_buffer.buff = buffer;
    adc_context.sample_buffer.status = NOT_FULL;
    adc_context.adc_mode = FREE_RUNNING;
    adc_enable_auto_triggering();
    adc_enable_interrupt();
    adc_start_conversion(); // starts conversion and with that enables free running mode
}

void adc_enable(void)
{
    SET_REG_BIT(ADCSRA, ADEN);
}

void adc_enable_interrupt(void)
{
    SET_REG_BIT(ADCSRA, ADIE);
}

void adc_disable_interrupt(void)
{
    CLR_REG_BIT(ADCSRA, ADIE);
}

void adc_start_conversion(void)
{
    SET_REG_BIT(ADCSRA, ADSC);
}

void adc_enable_auto_triggering(void)
{
    SET_REG_BIT(ADCSRA, ADATE);
}

void adc_disable_auto_triggering(void)
{
    CLR_REG_BIT(ADCSRA, ADATE);
}
