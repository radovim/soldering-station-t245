#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "uart.h"
#include "ring_buffer.h"

static volatile uint8_t uart_buffer[UART_BUFFER_SIZE] = {0};
static t_RingBuffer uart_tx_buffer = {.buff = uart_buffer, .size = UART_BUFFER_SIZE, .head = 0, .tail = 0};

static inline void uart_tx_enable_interrupt()
{
    UCSR0B |= 1 << UDRIE0;
}

static inline void uart_tx_disable_interrupt()
{
    UCSR0B &= ~(1 << UDRIE0);
}

ISR(USART_UDRE_vect)
{
    if (ring_buffer_empty(&uart_tx_buffer) == BUFFER_STATE_NOT_EMPTY)
    {
        UDR0 = ring_buffer_get(&uart_tx_buffer);
    }
    else
    {
        uart_tx_disable_interrupt();
    }
}

void uart_init()
{
    // set baud rate
    UBRR0H = (uint8_t)(UBBR_VALUE >> 8);
    UBRR0L = (uint8_t)(UBBR_VALUE);

    // Enable receiver and transmitter
    UCSR0B |= (1 << TXEN0);

    // Set frame format: 8data, 2stop bits
    UCSR0C = (1 << USBS0) | (1 << UCSZ01) | (1 << UCSZ00);
}

void uart_put_char(char c)
{
    while (ring_buffer_full(&uart_tx_buffer) == BUFFER_STATE_FULL); // Polling if tx buffer is full

    uart_tx_disable_interrupt();
    ring_buffer_put(&uart_tx_buffer, c);
    uart_tx_enable_interrupt();

    if (c == '\n')
    {
        uart_put_char('\r');
    }
}

void uart_put_string(char* str)
{
    char* message = str;
    while(*message)
    {
        uart_put_char(*message++);
    }
}
