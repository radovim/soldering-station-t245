#include <stdint.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "heater.h"
#include "adc.h"
#include "pid.h"
#include "encoder.h"
#include "timer.h"
#include "gpios.h"

#include <stdio.h>
#include "uart.h"

static uint16_t g_heater_start_temp = 180; // TODO store last target temp into eeprom and set that value as target temp on the next power-up
static uint16_t g_heater_real_temp;

// static void heater_set_duty_cycle(int input)
// {
//     if (input <= 0)
//     {
//         OCR1A = 0;
//     }
//     else
//     {
//         OCR1A = (input * MAX_DUTY_CYCLE) / MAX_INT;
//     }
// }

static void heater_turn_off()
{
    OCR1A = 255;
}

void heater_init(void)
{
    // Pin PB1 is connected to the gait of transistor which controls heater power
    // Configure 8-bit Fast PWM on port B pin 1
    TIMSK1 = 0; // Disable all timer 1 interrupts
    TCCR1B = 0; // Stop counter until init done
    TCCR1A = (1 << COM1A1) | (1 << WGM10);  // Clear OC1A on Compare Match, set OC1A at BOTTOM(inverting mode)
    TCCR1B = (1 << CS11) | (1 << WGM12);   // Set internal clock as clock source with no prescaling
    OCR1A = 127; // Set duty cycle to 0%
    gpio_set_dir(PORT_B, PIN_1, OUT); // set pin PB1 as output
}

float heater_measure_temp(void){
    uint16_t* samples = NULL;
    const uint8_t num_of_smps = 20;
    uint32_t sum_of_smps = 0;
    uint16_t smps_avg = 0;
    float real_temp = 0;
    uint8_t duty_cycle = OCR1A;

    heater_turn_off(); // turn off heater so temperature can be precisely measured
    _delay_ms(1); // wait PWM to settle

    samples = (uint16_t*)adc_get_samples(num_of_smps);

    OCR1A = duty_cycle;
    for (uint8_t i = 0; i < num_of_smps; i++)
    {
        sum_of_smps += samples[i];
    }

    if (num_of_smps != 0)
    {
        smps_avg = sum_of_smps / num_of_smps;
    }
    else
    {
        return 0;
    }

    if ((((1 << ADC_RESOLUTION_IN_BITS) - 1) != 0) && (TEMP_GAIN != 0))
    {
        // TODO: Avoid floating arithmetics
        float temp_sens_mv = (float)smps_avg / (float)((1 << ADC_RESOLUTION_IN_BITS) -1) * ADC_REF_VOLT * VOLTS_TO_MILLIVOLTS_FACTOR / TEMP_GAIN;
        real_temp = temp_sens_mv / K_TYPE_TC_FACTOR + 0.5f;
    }
    else
    {
        return 0;
    }

    g_heater_real_temp = real_temp;

    return real_temp;
}

uint8_t heater_heat(void)
{
    static uint32_t last_millis;
    uint32_t passed_millis = timer_get_millis() - last_millis;
    float Kp = 0;

    if (passed_millis >= PID_INTERVAL_IN_MS)
    {
        float measured_temp = heater_measure_temp();
        uint16_t target_temp = heater_get_target_temp();
        if (((uint16_t)measured_temp <= target_temp))
        {
            if ((uint16_t)measured_temp > 0)
            {
                Kp = (float)target_temp / measured_temp;
            }
            else 
            {
                Kp = 1;
            }
        }
        else
        {
            Kp = 0;
        }
        
        if (Kp >= 1)
        {
            // OCR1A = 255;
        }
        else
        {
            // OCR1A = 255 - (uint8_t)(255.0 * Kp);
            
        }
        // int16_t input_value = pid_controller(target_temp, measured_temp, &g_pidData);
        //heater_set_duty_cycle(duty_cycle);
        last_millis = timer_get_millis();
    }

    return 255 - OCR1A;
}

uint16_t heater_get_target_temp()
{
    return g_heater_start_temp + encoder_get_value();
}

uint16_t heater_get_real_temp()
{
    return g_heater_real_temp;
}