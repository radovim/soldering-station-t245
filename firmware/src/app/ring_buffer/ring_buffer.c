#include <stdint.h>
#include <stdlib.h>
#include "ring_buffer.h"

t_Result ring_buffer_put(t_RingBuffer* rb, const uint8_t new_data)
{
    t_Result result = ERROR;

    if (rb != NULL)
    {
        rb->buff[rb->head] = new_data;
        rb->head = (rb->head + 1) % rb->size;
        result = OK;
    }

    return result;
}

uint8_t ring_buffer_get(t_RingBuffer* rb)
{
    uint8_t data = 0;

    if (rb != NULL)
    {
        if (ring_buffer_empty(rb) == BUFFER_STATE_NOT_EMPTY)
        {
            data = (uint8_t)rb->buff[rb->tail];
            rb->tail = (rb->tail + 1) % rb->size;
        }
    }

    return data;
}

uint8_t ring_buffer_peek(t_RingBuffer* rb)
{
    uint8_t data = 0;

    if (rb != NULL)
    {
        data = (uint8_t)rb->buff[rb->tail];
    }

    return data;
}

t_BufferState ring_buffer_empty(t_RingBuffer* rb)
{
    t_BufferState is_empty = BUFFER_STATE_UNKNOWN;

    if (rb != NULL)
    {
        if (rb->head == rb->tail)
        {
            is_empty = BUFFER_STATE_EMPTY;
        }
        else
        {
            is_empty = BUFFER_STATE_NOT_EMPTY;
        }
    }

    return is_empty;
}

t_BufferState ring_buffer_full(t_RingBuffer* rb)
{
    t_BufferState is_full = BUFFER_STATE_UNKNOWN;

    if (rb != NULL)
    {
        if (rb->head == (rb->size - 1))
        {
            is_full = (rb->tail == 0) ? BUFFER_STATE_FULL : BUFFER_STATE_NOT_FULL;
        }
        else if ((rb->head + 1) == rb->tail)
        {
            is_full = BUFFER_STATE_FULL;
        }
        else
        {
            is_full = BUFFER_STATE_NOT_FULL;
        }
    }

    return is_full;
}