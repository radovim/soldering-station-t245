#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <stdint.h>
#include "types.h"


typedef struct
{
    volatile uint8_t* buff;
    uint8_t size;
    uint8_t tail;
    uint8_t head;
}t_RingBuffer;

typedef enum
{
    BUFFER_STATE_UNKNOWN = -1,
    BUFFER_STATE_NOT_EMPTY = 0,
    BUFFER_STATE_NOT_FULL = 0,
    BUFFER_STATE_EMPTY = 1,
    BUFFER_STATE_FULL = 1
}t_BufferState;

t_Result ring_buffer_put(t_RingBuffer* rb, const uint8_t data);
uint8_t ring_buffer_get(t_RingBuffer* rb);
uint8_t ring_buffer_peek(t_RingBuffer* rb);
t_BufferState ring_buffer_empty(t_RingBuffer* rb);
t_BufferState ring_buffer_full(t_RingBuffer* rb);

#endif /* RING_BUFFER_H */
